<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="ja">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous" />
    <link rel="stylesheet" href="css/style.css" type="text/css" />
    <link rel="stylesheet" href="css/login.css" type="text/css" />
    <title>ログイン画面</title>
</head>

<body>
    <form class="login-form mx-auto" action="Login" method="post">

        <div class="page-title-text">
            <h1>ログイン画面</h1>
        </div>

        <p class="statusMsg">${statusMsg}</p>

        <div class="form-group" style="text-align: center;">
            <input type="text" class="form-control" placeholder="ログインIDを入力" name="LoginID">
        </div>

        <div class="form-group">
            <input type="password" class="form-control" placeholder="パスワードを入力" name="Passwd">
        </div>

        <div class="loginBtn">
            <button class="btn btn-primary btn-block" type="submit">ログイン</button>
        </div>

    </form>
</body>

</html>