<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="jp">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous" />
    <link rel="stylesheet" href="css/style.css" type="text/css" />
    <link rel="stylesheet" href="css/userDelete.css" type="text/css" />
    <title>ユーザ削除</title>
</head>

<body>
    <div class="header">
        <div class="header_inner">
            <h1>Login:${loginUserInfo.name} さん</h1>
            <a href="Logout"><button type="button" class="btn btn-link">ログアウト</button></a>
        </div>
    </div>

    <div class="page-title-text">
        <h1>ユーザ削除確認</h1>
    </div>

    <div class="main-contents">
        <div class="dialog-msg">
            <h2>ログインID：${deleteUserDetail.loginId}</h2>
            <h2>を本当に削除してよろしいでしょうか。</h2>
        </div>
        <div class="dialog-btn">
        	<form action="UserDelete" method="post">
                <a class="btn btn-outline-dark" href="UserList">キャンセル</a>
                <button class="btn btn-danger" type="submit">OK</button>
        	</form>
        </div>

    </div>

</body>

</html>