<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html>
<html lang="jp">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous" />
    <link rel="stylesheet" href="css/style.css" type="text/css" />
    <!-- <link rel="stylesheet" href="css/userDetail.css" type="text/css" /> -->
    <title>ユーザ詳細情報</title>
</head>

<body>
    <div class="header">
        <div class="header_inner">
            <h1>Login:${loginUserInfo.name} さん</h1>
            <a href="Logout"><button type="button" class="btn btn-link">ログアウト</button></a>
        </div>
    </div>

    <div class="page-title-text">
        <h1>ユーザ詳細情報参照</h1>
    </div>

    <div class="main-contents">
        <form>
            <div class="form-group row">
                <label class="col-md-6 col-form-label">ログインID</label>
                <div class="col-md-6">
                	${userDetail.loginId}
                </div>
            </div>
            <hr>
            <div class="form-group row">
                <label class="col-md-6 col-form-label">ユーザ名</label>
                <div class="UserName col-md-6">
                    ${userDetail.name}
                </div>
            </div>
            <hr>
            <div class="form-group row">
                <label class="col-md-6 col-form-label">生年月日</label>
                <div class="BirthDate col-md-6">
                    <fmt:formatDate value="${userDetail.birthDate}" pattern="yyyy年MM月dd日" />
                </div>
            </div>
            <hr>
            <div class="form-group row">
                <label class="col-md-6 col-form-label">登録日時</label>
                <div class="CreateDate col-md-6">
                	<fmt:formatDate value="${userDetail.createDate}" pattern="yyyy年MM月dd日 HH:mm" />
                </div>
            </div>
            <hr>
            <div class="form-group row">
                <label class="col-md-6 col-form-label">更新日時</label>
                <div class="UpdateDate col-md-6">
                	<fmt:formatDate value="${userDetail.updateDate}" pattern="yyyy年MM月dd日 HH:mm" />
                </div>
            </div>

            <div class="page-back">
                <a href="UserList">戻る</a>
            </div>
        </form>
    </div>

</body>

</html>