<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html>
<html lang="jp">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous" />
    <link rel="stylesheet" href="css/style.css" type="text/css" />
    <link rel="stylesheet" href="css/userList.css" type="text/css" />
    <title>ユーザ一覧</title>
</head>

<body>
    <div class="header">
        <div class="header_inner">
            <h1>Login:${loginUserInfo.name} さん</h1>
            <a href="Logout"><button type="button" class="btn btn-link">ログアウト</button></a>
        </div>
    </div>

    <div class="page-title-text">
        <h1>ユーザ一覧</h1>
    </div>

    <div class="main-contents">
    	<c:if test="${loginUserInfo.loginId=='admin'}" var="flg" />

        <form class="create" action="UserCreate">
            <div class="create-Btn">
                <c:if test="${flg}"><button type="submit" class="btn btn-outline-primary btn-lg">新規登録</button></c:if>
            </div>
        </form>

        <form  class="search" action="UserList" method="post">
            <div class="form-group row">
                <label for="inputLoginID" class="col-md-2 col-form-label">ログインID</label>
                <div class="col-md-10">
                    <input type="text" class="form-control" id="inputLoginID" name="input-LoginID" value="">
                </div>
            </div>

            <div class="form-group row">
                <label for="inputUserNAME" class="col-md-2 col-form-label">ユーザ名</label>
                <div class="col-md-10">
                    <input type="text" class="form-control" id="inputUserNAME" name="input-UserNAME" value="">
                </div>
            </div>

            <div class="form-group row">
                <label for="inputDate" class="col-md-2 col-form-label">生年月日</label>
                <div class="col-md-10 form-inline">
                    <input type="date" class="form-control" name="input-birthDateA">
                    <div class="col-md-1 text-center">～</div>
                    <input type="date" class="form-control" name="input-birthDateZ">
                </div>
            </div>

            <div class="serch-Btn">
                <button type="submit" class="btn btn-primary">検索</button>
            </div>

        </form>

        <div class="userTable">
            <table class="table" cellspacing="0" cellpadding="5">
                <thead>
                    <tr class="thead-dark">
                        <th class="width20">ログインID</th>
                        <th class="width20">ユーザ名</th>
                        <th class="width20">生年月日</th>
                        <th class="width40"></th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach var="user" items="${userList}">
                        <tr>
                            <td>${user.loginId}</td>
                            <td>${user.name}</td>
                            <td><fmt:formatDate value="${user.birthDate}" pattern="yyyy年MM月dd日" /></td>
                            <td>
                                <c:if test="${flg}">
                                    <a class="btn btn-primary" href="UserDetail?id=${user.id}">詳細</a>
                                    <a class="btn btn-success" href="UserUpdate?id=${user.id}">更新</a>
                                    <a class="btn btn-danger" href="UserDelete?id=${user.id}">削除</a>
                                </c:if>
                                <c:if test="${!flg}">
                                    <a class="btn btn-primary" href="UserDetail?id=${user.id}">詳細</a>
                                    <c:if test="${user.loginId == loginUserInfo.loginId}">
                                        <a class="btn btn-success" href="UserUpdate?id=${user.id}">更新</a>
                                    </c:if>
                                </c:if>

                            </td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </div>

    </div>




</body>

</html>