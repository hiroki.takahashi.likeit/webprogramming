<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="jp">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous" />
    <link rel="stylesheet" href="css/style.css" type="text/css" />
    <!-- <link rel="stylesheet" href="css/userUpdate.css" type="text/css" /> -->
    <title>ユーザ情報更新</title>
</head>

<body>
    <div class="header">
        <div class="header_inner">
        	<%
        	String loginName = (String) request.getAttribute("loginName");
			%>
            <h1>Login:${loginUserInfo.name} さん</h1>
            <a href="Logout"><button type="button" class="btn btn-link">ログアウト</button></a>
        </div>
    </div>

    <div class="page-title-text">
        <h1>ユーザ情報更新</h1>
    </div>

    <div class="main-contents">
    	<p class="statusMsg"><c:out value="${statusMsg}"/></p>
        <form action="UserUpdate" method="post">
            <div class="form-group row vertical-align">
                <label for="LoginID" class="col-md-2 col-form-label">ログインID</label>
                <div class="col-md-6" id="LoginID">${updateUserDetail.loginId}</div>
            </div>
            <div class="form-group row">
                <label for="input-Passwd" class="col-md-2 col-form-label">パスワード</label>
                <div class="col-md-10">
                    <input type="password" class="form-control" id="input-Passwd" name="input-Passwd">
                </div>
            </div>
            <div class="form-group row">
                <label for="retype-Passwd" class="col-md-2 col-form-label">パスワード(確認)</label>
                <div class="col-md-10">
                    <input type="password" class="form-control" id="retype-Passwd" name="retype-Passwd">
                </div>
            </div>
            <div class="form-group row">
                <label for="input-UserName" class="col-md-2 col-form-label">ユーザ名</label>
                <div class="col-md-10">
                    <input type="text" class="form-control" id="input-UserName" name="input-UserName" value="${updateUserDetail.name}">
                </div>
            </div>
            <div class="form-group row">
                <label for="input-BirthDate" class="col-md-2 col-form-label">生年月日</label>
                <div class="col-md-10">
                    <input type="date" class="form-control" id="input-BirthDate" name="input-BirthDate" value="<fmt:formatDate value="${updateUserDetail.birthDate}" pattern="yyyy/MM/dd" />">
                </div>
            </div>

            <div class="submit-Btn text-center">
                <button class="btn btn-primary btn-lg" type="submit">更新</button>
            </div>

            <div class="page-back">
                <a href="UserList">戻る</a>
            </div>
        </form>
    </div>

</body>

</html>