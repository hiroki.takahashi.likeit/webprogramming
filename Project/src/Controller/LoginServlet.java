package Controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;
import model.common;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/Login")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// ログインセッションの確認
		if (common.loginSessionCheck(request, response)) {
			// セッション有場合
			response.sendRedirect("UserList");
		} else {
			// ログインセッション無場合
			// フォワード
			request.setAttribute("statusMsg", "");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/index.jsp");
			dispatcher.forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");

		// フォーム上のID,Passの取得
		String loginId = request.getParameter("LoginID");
		String password = request.getParameter("Passwd");

		// パスワードの暗号化解決
		String result = common.encryptForPassword(password);

		// DB接続,ユーザー取得
		UserDao userDao = new UserDao();
		User user = userDao.findByLoginInfo(loginId, result);


		if (user == null) {
			// 見つからなかったとき
			request.setAttribute("statusMsg", "ログインIDまたはパスワードが異なります");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/index.jsp");
			dispatcher.forward(request, response);
		} else {
			// 見つかった時
			// ログインセッション
			HttpSession session = request.getSession();
			session.setAttribute("loginUserInfo", user);

			// ページ移動
			response.sendRedirect("UserList");
		}
	}

}
