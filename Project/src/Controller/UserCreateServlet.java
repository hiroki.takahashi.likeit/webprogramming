package Controller;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.UserDao;
import model.User;
import model.common;

/**
 * Servlet implementation class userCreateServlet
 */
@WebServlet("/UserCreate")
public class UserCreateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserCreateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// セッション有無チェック
		boolean sessionFlg = common.loginSessionCheck(request, response);

		if (sessionFlg == false) {
			// セッション無し
			response.sendRedirect("Login");
		} else {
			// セッション有り

			// ユーザ権限チェック
			boolean adminUserFlg = common.adminUserCheck(request, response);

			if (adminUserFlg == false) {
				// 権限なし
				response.sendRedirect("AccessAuthorityError");
			}else {
				// 権限あり
				// フォワード
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userCreate.jsp");
				dispatcher.forward(request, response);
			}
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// ユーザ登録機能

		request.setCharacterEncoding("UTF-8");

		// 登録確認用
		int recodeIndex = 0;

		// フォーム上のデータを取得
		String loginId = request.getParameter("input-LoginID");
		String password = request.getParameter("input-Passwd");
		String rePassword = request.getParameter("retype-Passwd");
		String userName = request.getParameter("input-UserName");
		String birthDateStr = request.getParameter("input-BirthDate");



		// 作成日時、更新日時を現在時刻で取得
		Timestamp createDate = new Timestamp(System.currentTimeMillis());
		Timestamp updateDate = new Timestamp(System.currentTimeMillis());

		// 登録データの整合性チェック用
		boolean integrityFlg = true;

		// フォーム未入力チェック
		String[] params = {loginId, password, rePassword, userName, birthDateStr};
		for (String values : params) {
			if(values.isEmpty()) {
				// 空あり
				integrityFlg = false;
//				System.out.println("NG_flg:フォーム未入力");
				break;
			}
		}

		// ログインID重複チェック
		UserDao userDao = new UserDao();
		User user = userDao.findBy("login_id", loginId);

		if (user != null) {
			// 重複有り
			integrityFlg = false;
//			System.out.println("NG_flg:ログインID重複");
		}

		// パスワード一致確認
		if(!(password.equals(rePassword))) {
			integrityFlg = false;
//			System.out.println("NG_flg:パスワード不一致");
		}

		// データ整合性フラグチェック
		if (integrityFlg == true) {
			// OK:ユーザの登録処理
			// パスワードの暗号化
			String result = common.encryptForPassword(password);

			// 誕生日を日付型に変換
			Date birthDate = common.formatStringToDate(birthDateStr);

			// 登録ユーザの作成
			User createUser = new User(loginId, userName, birthDate, result, createDate, updateDate);

			// Doa 呼び出し
			recodeIndex = userDao.createUser(createUser);
		}

		// 登録チェック
		if (recodeIndex > 0) {
			// OK:ユーザ一覧画面へ移動
			response.sendRedirect("UserList");
		} else if(recodeIndex == 0) {
			// NG:パラメータを引き継いで登録画面を再表示
			// 引き継ぐ要素
			request.setAttribute("loginId", loginId);
			request.setAttribute("userName", userName);
			request.setAttribute("birthDate", birthDateStr);

			// 失敗メッセージ
			request.setAttribute("statusMsg", "入力された内容は正しくありません");

			// 更新画面を表示
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userCreate.jsp");
			dispatcher.forward(request, response);
		}
	}
}
