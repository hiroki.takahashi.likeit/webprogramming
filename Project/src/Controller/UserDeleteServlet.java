package Controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;
import model.common;

/**
 * Servlet implementation class userDeleteServlet
 */
@WebServlet("/UserDelete")
public class UserDeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserDeleteServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// セッション有無チェック
		boolean sessionFlg = common.loginSessionCheck(request, response);

		if (sessionFlg == false) {
			// セッション無いときはログイン画面へ
			response.sendRedirect("Login");
		} else {
			// ユーザ権限チェック
			boolean adminUserFlg = common.adminUserCheck(request, response);

			if (adminUserFlg == false) {
				// 権限なし
				response.sendRedirect("AccessAuthorityError");
			} else {
				// 権限あり

				String targetId = request.getParameter("id");

				UserDao userDao = new UserDao();
				User user = userDao.findBy("id", targetId);

				HttpSession session = request.getSession();
				session.setAttribute("deleteUserDetail", user);

				// フォワード
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userDelete.jsp");
				dispatcher.forward(request, response);
			}
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// ユーザ削除

		// セッション情報の取得
		HttpSession session = request.getSession();
		User user = (User)session.getAttribute("deleteUserDetail");

		// Dao呼び出し:ユーザ削除
		UserDao userDao = new UserDao();
		int deleteIndex = userDao.deleteUser(user);

		if (deleteIndex > 0) {
			session.removeAttribute("deleteUserDetail");
			response.sendRedirect("UserList");
		}
	}

}
