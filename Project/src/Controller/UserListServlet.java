package Controller;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.UserDao;
import model.User;
import model.common;

/**
 * Servlet implementation class userListServlet
 */
@WebServlet("/UserList")
public class UserListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserListServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// セッション有無チェック
		boolean sessionFlg = common.loginSessionCheck(request, response);

		if (sessionFlg == false) {
			response.sendRedirect("Login");
		} else {
			// 全ユーザ情報取得
			UserDao userDao = new UserDao();
			List<User> userList = userDao.findAll();

			request.setAttribute("userList", userList);

			// フォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userList.jsp");
			dispatcher.forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// 検索機能

		// 文字コードセット
		request.setCharacterEncoding("UTF-8");

		// フォーム上のデータを取得
		String loginId = request.getParameter("input-LoginID");
		String userName = request.getParameter("input-UserNAME");
		String birthDateAstr = request.getParameter("input-birthDateA");
		String birthDateZstr = request.getParameter("input-birthDateZ");

		// 日付空のとき
		if (birthDateAstr.isEmpty()) {
			birthDateAstr = "1900-1-1";
		}
		if (birthDateZstr.isEmpty()) {
			birthDateZstr = "2100-1-1";
		}

		// 文字列型から日付型へ変換
		Date birthDateA = common.formatStringToDate(birthDateAstr);
		Date birthDateZ = common.formatStringToDate(birthDateZstr);

		// ユーザリストの作成
		UserDao userDao = new UserDao();
		List<User> userList = userDao.findUser(loginId, userName, birthDateA, birthDateZ);

		request.setAttribute("userList", userList);

		// フォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userList.jsp");
		dispatcher.forward(request, response);
	}

}