package Controller;

import java.io.IOException;
import java.sql.Timestamp;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;
import model.common;

/**
 * Servlet implementation class userUpdateServlet
 */
@WebServlet("/UserUpdate")
public class UserUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserUpdateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// セッション有無チェック
		boolean sessionFlg = common.loginSessionCheck(request, response);

		if (sessionFlg == false) {
			response.sendRedirect("Login");
		} else {

			// ユーザ権限チェック
//			boolean AccessAuthorityFlg = common.AccessAuthorityCheck(request, response);
			boolean adminUserFlg = common.adminUserCheck(request, response);
			boolean targetUserFlg = common.targetMyselfCheck(request, response);

//			if (AccessAuthorityFlg == false) {
			if ((!adminUserFlg) && (targetUserFlg)) {
				// 権限なし
				response.sendRedirect("AccessAuthorityError");
			} else {
				// 権限あり
				// 選択されたユーザのIDを取得
				String targetid = request.getParameter("id");
				// IDからユーザ情報を検索
				UserDao userDao = new UserDao();
				User user = userDao.findBy("id", targetid);

				HttpSession session = request.getSession();
				session.setAttribute("updateUserDetail", user);
				// フォワード
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
				dispatcher.forward(request, response);
			}
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// ユーザ登録機能
//		System.out.println("start update");

		// 更新対象のユーザ情報取得
		HttpSession session = request.getSession();
		User updateUserDetail = (User)session.getAttribute("updateUserDetail");

		request.setCharacterEncoding("UTF-8");

		int updateIndex = 0;

		// フォーム上のデータを取得
		String password = request.getParameter("input-Passwd");
		String rePassword = request.getParameter("retype-Passwd");
		String userName = request.getParameter("input-UserName");
		String birthDateStr = request.getParameter("input-BirthDate");

		// 作成日時、更新日時を現在時刻で取得
		Timestamp updateDate = new Timestamp(System.currentTimeMillis());

		// 登録データの整合性チェック用
		boolean integrityFlg = true;

		// フォーム未入力チェック
		String[] params = {userName, birthDateStr};
		for (String values : params) {
			if(values.isEmpty()) {
				integrityFlg = false;
//				System.out.println("NG_flg:1");
				break;
			}
		}

		// パスワードの入力チェック
		if (password.isEmpty() && rePassword.isEmpty()) {
			// パスワード未入力のとき

			// 整合性フラグチェック
			if(integrityFlg == true) {
				// 更新:名前、誕生日、更新日時
				updateUserDetail.setName(userName);
				updateUserDetail.setBirthDate(common.formatStringToDate(birthDateStr));
				updateUserDetail.setUpdateDate(updateDate);

				// Doa 呼び出し
				UserDao userDao = new UserDao();
				updateIndex = userDao.updateUser(updateUserDetail);
			}
		} else {
			// パスワード入力の時

			// パスワード一致確認
			if(!(password.equals(rePassword))) {
				integrityFlg = false;
//				System.out.println("NG_flg:2");
			}

			// 整合性フラグチェック
			if (integrityFlg == true) {
				// OK:ユーザの登録処理
				// パスワードの暗号化
				String result = common.encryptForPassword(password);

				// 更新:パスワード、名前、誕生日、更新日時
				updateUserDetail.setPassword(result);
				updateUserDetail.setName(userName);
				updateUserDetail.setBirthDate(common.formatStringToDate(birthDateStr));
				updateUserDetail.setUpdateDate(updateDate);

				// Doa 呼び出し
				UserDao userDao = new UserDao();
				updateIndex = userDao.updateUser(updateUserDetail);

			}
		}

		// 成否チェック
		if (updateIndex > 0) {
			// OK
			// アップデートユーザのセッション情報を破棄
			session.removeAttribute("updateUserDetail");
			// ページ戻り
			response.sendRedirect("UserList");
		} else if(updateIndex == 0) {
			// NG:登録画面を再表示
			// 失敗メッセージ
			request.setAttribute("statusMsg", "入力された内容は正しくありません");

			// 更新画面再表示
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
			dispatcher.forward(request, response);
		}
	}
}
