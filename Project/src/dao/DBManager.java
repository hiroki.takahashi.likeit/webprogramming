package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

//DB接続処理

public class DBManager {

	final private static String DB_URL = "jdbc:mysql://localhost/";
	final private static String DB_NAME = "usermanagement";
	final private static String PARAMETERS = "?useUnicode=true&characterEncoding=utf8";
	final private static String USER = "root";
	final private static String PASS = "hiroki505";

	public static Connection getConnection() {

		Connection conn = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			conn = DriverManager.getConnection(DB_URL + DB_NAME + PARAMETERS, USER, PASS);

		} catch(SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}
		return conn;
	}
}
