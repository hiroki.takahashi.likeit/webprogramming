package dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
//import java.util.Date;
import java.util.List;

import model.User;
import model.common;

public class UserDao {

	/**
	 * ログインユーザの検索
	 */
	public User findByLoginInfo(String loginId, String password) {
		// 変数初期化
		Connection conn = null;

		try {
			// コネクション確立
			conn = DBManager.getConnection();

			// SQL文
			String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";

			// パラメータ設定
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			pStmt.setString(2, password);
			// 実行
			ResultSet rs = pStmt.executeQuery();

			// 一回だけ実行
			if (!rs.next()) {
				return null;
			}

			// ヒットしたデータをインスタンスにセット
			String loginIdData = rs.getString("login_id");
			String nameData = rs.getString("name");
			return new User(loginIdData, nameData);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}


	/**
	 * 全ユーザを検索してユーザ型のリストとして返す
	 * adminユーザは対象から除外
	 */
	public List<User> findAll() {
		// 変数初期化
		Connection conn = null;
		List<User> userList = new ArrayList<User>();

		try {
			conn = DBManager.getConnection();

			String sql = "SELECT * FROM user WHERE login_id not in ('admin')";

			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("password");
				Timestamp createDate = rs.getTimestamp("create_date");
				Timestamp updateDate = rs.getTimestamp("update_date");

				User user = new User(id, loginId, name, birthDate, password, createDate, updateDate);

				userList.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;
	}

	/**
	 * ユーザ検索用
	 * ログインID、名前、生年月日からヒットしたユーザのリストを返す
	 * admin除外
	 */
	public List<User> findUser(String targetLoginId,
								String targetName,
								java.util.Date birthDateA,
								java.util.Date birthDateZ){

		// 変数初期化
		Connection conn = null;
		List<User> userList = new ArrayList<User>();

		try {
			// コネクション確立
			conn = DBManager.getConnection();

			// SQL文
			String sql = "SELECT * FROM user WHERE login_id not in ('admin') "
					+ "AND CASE WHEN ? = '' THEN '' ELSE login_id END = ? "	// 入力有り:完全一致、無し:全検索
					+ "AND name LIKE ? "									// 部分一致検索
					+ "AND birth_date BETWEEN ? AND ?";						// 範囲検索

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, targetLoginId);
			pStmt.setString(2, targetLoginId);
			pStmt.setString(3, "%" + targetName + "%");
			pStmt.setDate(4, common.convUtilDate(birthDateA));
			pStmt.setDate(5, common.convUtilDate(birthDateZ));

			ResultSet rs = pStmt.executeQuery();

			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("password");
				Timestamp createDate = rs.getTimestamp("create_date");
				Timestamp updateDate = rs.getTimestamp("update_date");

				User user = new User(id, loginId, name, birthDate, password, createDate, updateDate);

				userList.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;
	}

	/**
	 * 与えられたカラムで完全一致するユーザを検索し返す
	 */
	public User findBy(String targetColumn, String targetData) {
		Connection conn = null;
		try {
			// DB接続
			conn = DBManager.getConnection();

			// SQL文
			String sql = "SELECT * FROM user WHERE " + targetColumn + " = ?";

			// SQL実行＆結果の取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, targetData);
			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return null;
			}

			int id = rs.getInt("id");
			String loginId = rs.getString("login_id");
			String name = rs.getString("name");
			Date birthDate = rs.getDate("birth_date");
			String password = rs.getString("password");
			Timestamp createDate = rs.getTimestamp("create_date");
			Timestamp updateDate = rs.getTimestamp("update_date");

			User user = new User(id, loginId, name, birthDate, password, createDate, updateDate);

			return user;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	/**
	 * 与えられたユーザ情報をDBへ登録する
	 */
	public int createUser(User targetUser) {
		Connection conn = null;
		try {
			// DB接続
			conn = DBManager.getConnection();

			// SQL文
			String sql =  "INSERT INTO "
							+ "user("
								+ "login_id, "
								+ "name, "
								+ "birth_date, "
								+ "password, "
								+ "create_date, "
								+ "update_date) "
							+ "VALUE(?, ?, ?, ?, ?, ?)";


			// SQL実行＆結果の取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, targetUser.getLoginId());
			pStmt.setString(2, targetUser.getName());
			pStmt.setDate(3, common.convUtilDate(targetUser.getBirthDate()));
			pStmt.setString(4, targetUser.getPassword());
			pStmt.setTimestamp(5, targetUser.getCreateDate());
			pStmt.setTimestamp(6, targetUser.getUpdateDate());
			int rs = pStmt.executeUpdate();

			return rs;

		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

	}

	/**
	 * 与えられたユーザのIDに一致するユーザ情報を更新
	 */
	public int updateUser(User targetUser) {
		Connection conn = null;
		try {
			// DB接続
			conn = DBManager.getConnection();

			// SQL文
			String sql = "UPDATE user SET "
					+ "name = ?,"
					+ "password = ?,"
					+ "birth_date = ?,"
					+ "update_date = ?"
					+ " WHERE id = ?";

			// SQL実行＆結果の取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, targetUser.getName());
			pStmt.setString(2, targetUser.getPassword());
			pStmt.setDate(3, common.convUtilDate(targetUser.getBirthDate()));
			pStmt.setTimestamp(4, targetUser.getUpdateDate());
			pStmt.setInt(5, targetUser.getId());

			int updateIndex = pStmt.executeUpdate();

			//--終了--
			return updateIndex;

		} catch (SQLException e) {
			e.printStackTrace();
			return 0;

		} finally {

			// DB切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return 0;
				}
			}
		}
	}

	/**
	 * 与えられたユーザのIDに一致するユーザ情報を削除
	 */
	public int deleteUser(User targetUser) {
		Connection conn = null;
		try {
			// DB接続
			conn = DBManager.getConnection();

			// SQL文
			String sql = "DELETE FROM user WHERE id = ?";

			// SQL実行＆結果の取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1, targetUser.getId());
			int deleteIndex = pStmt.executeUpdate();

			return deleteIndex;

			//--終了--

		} catch (SQLException e) {
			e.printStackTrace();
			return 0;

		} finally {

			// DB切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return 0;
				}
			}
		}
	}
}