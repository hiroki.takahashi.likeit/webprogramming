package model;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.bind.DatatypeConverter;

import dao.UserDao;

public class common {

	/**
	 * ログインセッションがあればtrue、なければfalseを返す
	 */
	public static boolean loginSessionCheck(HttpServletRequest request,
			HttpServletResponse response) {

		boolean sessionFlg;

		// セッション情報の取得
		HttpSession session = request.getSession();
		User user = (User)session.getAttribute("loginUserInfo");

		if (user == null) {
			sessionFlg = false;	// セッション無し
		} else {
			sessionFlg = true;	// セッション有り
		}

		return sessionFlg;
	}

	/**
	 * ログインユーザが管理者か、
	 * またはログインユーザと操作対象ユーザが一致しているかをチェックし
	 * 管理者または一致していればtrue、不一致であればfalseを返す
	 */
	public static boolean AccessAuthorityCheck(HttpServletRequest request,
			HttpServletResponse response) {

		boolean AccessAuthorityFlg;
		String adminUserId = "admin";
		String operetionTargetId = request.getParameter("id");

		// セッション情報からログインユーザを取得
		HttpSession session = request.getSession();
		User loginUser = (User)session.getAttribute("loginUserInfo");

		// 操作ターゲットユーザを取得
		UserDao userDao = new UserDao();
		User operetionTargetUser = userDao.findBy("id", operetionTargetId);

		if(loginUser.getLoginId().equals(adminUserId) ||
		   loginUser.getLoginId().equals(operetionTargetUser.getLoginId())) {
			AccessAuthorityFlg = true;	// 権限あり
		} else {
			AccessAuthorityFlg = false;	// 権限なし
		}
		return AccessAuthorityFlg;
	}

	/**
	 * ログインユーザが管理者かをチェックし
	 * 管理者であればtrue、不一致であればfalseを返す
	 */
	public static boolean adminUserCheck(HttpServletRequest request,
			HttpServletResponse response) {

		boolean adminUserFlg;
		String adminUserId = "admin";

		// セッション情報からログインユーザを取得
		HttpSession session = request.getSession();
		User loginUser = (User)session.getAttribute("loginUserInfo");

		if(loginUser.getLoginId().equals(adminUserId)){
			adminUserFlg = true;	// 権限あり
		} else {
			adminUserFlg = false;	// 権限なし
		}
		return adminUserFlg;
	}

	/**
	 * ログインユーザと操作対象ユーザが一致しているかをチェックし
	 * 一致していればtrue、不一致であればfalseを返す
	 */
	public static boolean targetMyselfCheck(HttpServletRequest request,
			HttpServletResponse response) {

		boolean targetMyselfFlg;
		String operetionTargetId = request.getParameter("id");

		// セッション情報からログインユーザを取得
		HttpSession session = request.getSession();
		User loginUser = (User)session.getAttribute("loginUserInfo");

		// 操作ターゲットユーザを取得
		UserDao userDao = new UserDao();
		User operetionTargetUser = userDao.findBy("id", operetionTargetId);

		if(loginUser.getLoginId().equals(operetionTargetUser.getLoginId())) {
			targetMyselfFlg = true;	// 権限あり
		} else {
			targetMyselfFlg = false;	// 権限なし
		}
		return targetMyselfFlg;
	}


	/**
	 * 文字列型から日付型へ変換
	 */
	public static Date formatStringToDate(String targetDate) {
//		System.out.println("start to formatStringToDate:" + targetDate);

		Date formatedDate = null;

		String str = targetDate.replace("/", "-");

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            formatedDate = sdf.parse(str);

        } catch(ParseException e) {
            e.printStackTrace();
        }
		return formatedDate;
	}

	/**
	 * 受け取ったutil.Date型をsql.Date型に変換して返す
	 */
	public static java.sql.Date convUtilDate(java.util.Date utilDate) {
        return new java.sql.Date(utilDate.getTime());
    }

	/**
	 * 受け取った文字列をMD5で暗号化して返す
	 */
	public static String encryptForPassword(String source) {

		String result = null;
		Charset charset = StandardCharsets.UTF_8;
		String algorithm = "MD5";

		byte[] bytes = null;
		try {
			bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
		} catch (NoSuchAlgorithmException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}

		result = DatatypeConverter.printHexBinary(bytes);
		return result;
	}
}