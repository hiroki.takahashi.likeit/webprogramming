CREATE TABLE
    user(
        id SERIAL AUTO_INCREMENT PRIMARY KEY,
        login_id varchar(255) UNIQUE NOT NULL, 
        name varchar(255) NOT NULL,
        birth_date DATE NOT NULL,
        password VARCHAR(255) NOT NULL,
        create_date DATETIME NOT NULL,
        update_date DATETIME NOT NULL
    );

INSERT INTO 
    user VALUES(
        1,
        'admin',
        '管理者',
        '1980/04/01',
        MD5('admin'),
        sysdate(),
        sysdate()
    );

INSERT INTO 
    user VALUES(
        2,
        'user1',
        '横浜流星',
        '1996/09/16',
        MD5('user1'),
        sysdate(),
        sysdate()
    );
INSERT INTO 
    user VALUES(
        3,
        'user2',
        '吉沢亮',
        '1994/02/01',
        MD5('user2'),
        sysdate(),
        sysdate()
    );
INSERT INTO 
    user VALUES(
        4,
        'user3',
        '新田真剣佑',
        '1996/11/16',
        MD5('user3'),
        sysdate(),
        sysdate()
    );